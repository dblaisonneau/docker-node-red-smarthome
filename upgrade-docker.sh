docker build -t registry.gitlab.com/dblaisonneau/docker-node-red-smarthome . 
docker push registry.gitlab.com/dblaisonneau/docker-node-red-smarthome
cd ../hobbitebourg/docker/iot/ 
docker-compose stop smarthome
docker-compose rm smarthome
docker rmi registry.gitlab.com/dblaisonneau/docker-node-red-smarthome:latest
docker-compose up -d && docker-compose logs smarthome
