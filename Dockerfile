FROM nodered/node-red-docker:v8
USER root
WORKDIR /usr/src/node-red

# ---- ZWAVE ----

# Install latest OpenZwave library
RUN apt-get update \
    && apt-get -y install libudev-dev beep \
    && mkdir -pv /usr/src/ \
    && git clone https://github.com/OpenZWave/open-zwave.git /usr/src/open-zwave \
    && cd /usr/src/open-zwave && make && make install
ENV LD_LIBRARY_PATH /usr/local/lib64
RUN ldconfig /usr/local/lib64

# Add openzwave nodes
RUN npm i -S node-red-node-serialport\
    && npm i -S node-red-contrib-openzwave \
    && npm i -S node-red-contrib-sunevents \
    && npm i -S node-red-contrib-scheduler \
    && npm i -S node-red-contrib-advanced-ping \
    && npm i -S node-red-contrib-repeat \
    && npm i -S node-red-contrib-influxdb \
    && npm i -S node-red-contrib-splitter \
    && npm i -S node-red-node-geohash \
    && npm i -S node-red-contrib-node-hue \
    && npm i -S node-red-dashboard \
    && npm i -S node-red-contrib-mjpgcamera \
    && npm i -S node-red-contrib-boolean-logic \
    && npm i -S node-red-contrib-actionflows \
    && npm i -S node-red-contrib-ttn
#     && npm i -S node-red-contrib-web-push
# --------------------------------

# User configuration directory volume
EXPOSE 1880

# Environment variable holding file path for flows configuration
ENV FLOWS=flows.json

CMD npm start -- --userDir /data
